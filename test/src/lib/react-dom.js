import { TEXT } from "./share/const";

function render(vnode, container) {
    console.log('vnode', vnode);
  if (typeof vnode == "string") {
    vnode = {
      type: TEXT,
      props: {
        children: [],
        nodeValue: vnode,
      },
    };
  }
  const node = createNode(vnode);
  container.appendChild(node);
}
function createNode(vnode) {
  const { type, props } = vnode;
  let node;
  if (type === TEXT) {
    node = document.createTextNode("");
  } else if (typeof type == "string") {
    node = document.createElement(type);
  } else if(typeof type == 'function') {
      if(type.toString().indexOf('class')==0) {
          const cmp = new type(props);
        const vnode = cmp.render();
        return createNode(vnode);
      } else {
        node = createNode(type.call(null, props));
        return node;
      }
    
  } 

  reconcileChildren(props.children, node);
  updateNode(node, props);

  return node;
}

function reconcileChildren(children, node) {
  for (let i = 0; i < children.length; i++) {
    let child = children[i];
    render(child, node);
  }
}
// 属性值 attribute
function updateNode(node, nextVal) {
  Object.keys(nextVal)
    .filter((k) => k !== "children")
    .forEach((k) => {
      node[k] = nextVal[k];
    });
}

export default {
  render,
};
