import { TEXT } from "./share/const";

function createElement(type, config, ...children) {
  delete config._owner;
  delete config._ref;
  console.log("children", children);
  const props = {
    ...config,
    children: children.map((child) =>
      typeof child === "object" ? child : createTextNode(child)
    ),
  };
  return {
    type,
    props,
  };
}
function createTextNode(text) {
  return {
    type: TEXT,
    props: {
      children: [],
      nodeValue: text,
    },
  };
}
export default {
  createElement,
};
