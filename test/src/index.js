import React from "./lib/";
// import ReactDOM from "./lib/react-dom";
import ReactDOM from "./lib-fiber-hook/react-dom";
import { useState } from './lib-fiber-hook/hook';

import "./index.css";

function Header(props) {
  // const [count, setCount] = useState(0);
  const count = 1;
  const setCount = ()=>{}
  return <div className="myclass">
    <button onClick={()=>setCount(count+1)}>set count</button>
    <p>{count}</p></div>;
}
const jsx = (
  <div className="myclass">
    <p>
      <p><p>aaa</p></p>
      <Header name="hihi" />
    </p>
    <a href="www.baidu.com">baidu</a>
    {/* <App></App> */}
  </div>
);
ReactDOM.render(jsx, document.getElementById("root"));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
