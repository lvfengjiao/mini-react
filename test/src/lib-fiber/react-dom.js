let nextUnitWork = null;
let wipRoot = null;
let wip = null;
function render(vnode, container) {
  wipRoot = {
    stateNode: container,
    type: container.nodeName.toLocaleLowerCase(),
    props: { children: vnode },
    child: null,
    // 下一个兄弟节点 fiber
    sibling: null,
  };
  wip = wipRoot;
  nextUnitWork = wip;
}
function commitWork(fiber) {
  if (fiber == null) return;
  if (fiber.stateNode) {
    const parentDom = getParentNode(fiber.return);
    parentDom.appendChild(fiber.stateNode);
    commitWork(fiber.child);
    commitWork(fiber.sibling);
  }
}

function commitRoot() {
  commitWork(wipRoot.child);
}
function createFiber(vnode, wip) {
  return {
    type: vnode.type,
    props: vnode.props,
    stateNode: null,
    return: wip,
    child: null,
    // 下一个兄弟节点 fiber
    sibling: null,
  };
}
function getParentNode(wip) {
  let tem = wip;
  while (tem) {
    if (tem.stateNode) {
      return tem.stateNode;
    }
    tem = tem.return;
  }
}
function reconcileChildren(wip, children) {
  children = Array.isArray(children) ? children : [children];
  let previousWip = null;
  for (let i = 0; i < children.length; i++) {
    const vnode = children[i];
    const currentFiber = createFiber(vnode, wip);
    if (previousWip == null) {
      wip.child = currentFiber;
    } else {
      previousWip.sibling = currentFiber;
    }
    previousWip = currentFiber;
  }
}
function updateNode(node, props) {
  Object.keys(props)
    .forEach((key) => {
      if(key==='children') {
        if(typeof props[key]==='number' ||typeof props[key]==='string' ){
          node.textContent = props[key];
        }
      } else node[key] = props[key];
    });
}
function createNode(wip) {
  console.log({type:wip.type});
  let node = document.createElement(wip.type);
  if(wip.props) updateNode(node, wip.props);
  return node;
}
function updateHostComponent(wip) {
  const { stateNode, props } = wip;
  if (!stateNode) {
    wip.stateNode = createNode(wip);
  }
  if (props) reconcileChildren(wip, props.children);
}
function performUnitWork(nextUnitWork) {
  updateHostComponent(nextUnitWork);
  if (nextUnitWork.child) {
    return nextUnitWork.child;
  } 
  let next = nextUnitWork;
  while (next) {
    if (next.sibling) {
      return next.sibling;
    }
    next = next.return;
  }
  return null;
}
function workLoop(deadline) {
  while (nextUnitWork && deadline.timeRemaining() > 0) {
    console.log({nextUnitWork}, 'befoer');
    nextUnitWork = performUnitWork(nextUnitWork);
    console.log({nextUnitWork}, 'after');
  }

  if (nextUnitWork == null) {
    commitRoot();
  }
}
window.requestIdleCallback(workLoop);
export default {
  render,
};
