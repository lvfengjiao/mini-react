import { renderHooks } from "./hook";

let nextUnitWork = null;
let wipRoot = null;
let wip = null;
export const scheduleUpdateOnFiber = (fiber) => {
  fiber.alternate = { ...fiber };
  wip = fiber;
  wipRoot = wip;
  wipRoot.sibling = null;
  nextUnitWork = wip;
};
function render(vnode, container) {
  const wipRoot = {
    stateNode: container,
    type: container.nodeName.toLocaleLowerCase(),
    props: { children: vnode },
    child: null,
    // 下一个兄弟节点 fiber
    sibling: null,
  };

  scheduleUpdateOnFiber(wipRoot);
}
function commitWork(fiber) {
  if (fiber == null) return;
  if (fiber.stateNode) {
    const parentDom = getParentNode(fiber.return);
    parentDom.appendChild(fiber.stateNode);
    commitWork(fiber.child);
    commitWork(fiber.sibling);
  }
}

function commitRoot() {
  typeof wipRoot.type === "function"
    ? commitWork(wipRoot)
    : commitWork(wipRoot.child);
  wipRoot = null;
}
function createFiber(vnode, wip) {
  return {
    type: vnode.type,
    props: vnode.props,
    stateNode: null,
    return: wip,
    child: null,
    // 下一个兄弟节点 fiber
    sibling: null,
  };
}
function getParentNode(wip) {
  let tem = wip;
  while (tem) {
    if (tem.stateNode) {
      return tem.stateNode;
    }
    tem = tem.return;
  }
}
function reconcileChildren(wip, children) {
    if (typeof children === 'string') {
        return;
      }
    children = Array.isArray(children) ? children : [children];
  let previousWip = null;
  for (let i = 0; i < children.length; i++) {
    const vnode = children[i];
    if(vnode==null) continue;
    const currentFiber = createFiber(vnode, wip);
    if (previousWip == null) {
      wip.child = currentFiber;
    } else {
      previousWip.sibling = currentFiber;
    }
    previousWip = currentFiber;
  }
}
function updateNode(node, props) {
  Object.keys(props).forEach((key) => {
    if (key === "children") {
      if (key.startsWith("on")) {
        const eventName = key.slice(2).toLocaleLowerCase();
        node.addEventListener(eventName, props[key]);
      } else {
        if (typeof props[key] === "number" || typeof props[key] === "string") {
          node.textContent = props[key];
        }
      }
    } else node[key] = props[key];
  });
}
function createNode(wip) {
  let node = document.createElement(wip.type);
  if (wip.props) updateNode(node, wip.props);
  return node;
}
function updateHostComponent(wip) {
  const { stateNode, props } = wip;
  if (!stateNode) {
    wip.stateNode = createNode(wip);
  }
  if (props) reconcileChildren(wip, props.children);
}

function updateFunctionComponent(wip) {
//   renderHooks(wip);
  const { type, props } = wip;
  let vnode = type(props);
  let wipNode = createFiber(vnode, wip);
  updateHostComponent(wipNode);
//   reconcileChildren(wip, children);
}
function performUnitWork(nextUnitWork) {
  if (typeof nextUnitWork.type === "function") {
    updateFunctionComponent(nextUnitWork);
  } else if(typeof nextUnitWork.type == 'string'||typeof nextUnitWork.type==='number'){
    updateHostComponent(nextUnitWork);
  }
  if (nextUnitWork.child) {
    return nextUnitWork.child;
  }
  let next = nextUnitWork;
  while (next) {
    if (next.sibling) {
      return next.sibling;
    }
    next = next.return;
  }
  return null;
}
function workLoop(deadline) {
  while (nextUnitWork && deadline.timeRemaining() > 0) {
    // console.log({ nextUnitWork }, "befoer");
    nextUnitWork = performUnitWork(nextUnitWork);
    // console.log({ nextUnitWork }, "after");
  }
  window.requestIdleCallback(workLoop);

  if (nextUnitWork == null && wipRoot) {
    commitRoot();
  }
}
window.requestIdleCallback(workLoop);
export default {
  render,
};
