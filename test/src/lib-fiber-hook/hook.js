import {scheduleUpdateOnFiber} from './react-dom';
let currentRendierngFiber = null;
let workInProgressHook = null;

export function renderHooks(wip){
    currentRendierngFiber = wip;
    currentRendierngFiber.memorizedState = null;
    workInProgressHook = null;
}
// hook链表会在每一步获取memorisedState
function updateWorkInProgressHook(){
    let hook;
    const current = currentRendierngFiber.alternate;
    if(current){
        currentRendierngFiber.memorizedState = current.memorizedState; //获取当前的状态
        if(workInProgressHook){
            hook = workInProgressHook = workInProgressHook.next;
        } else {
            hook = workInProgressHook = current.memorizedState;
        }
    } else {
        hook = {
            memorizedState: null,
            next: null
        };
        if(workInProgressHook){
            workInProgressHook = workInProgressHook.next = hook;
        } else {
            workInProgressHook = currentRendierngFiber.memorizedState = hook;
        }
    }
    return hook;

}
export const useState = (initstate)=>{
    const hook = updateWorkInProgressHook();

    const fn = (state = initstate)=>{
        hook.memorizedState = state;
        scheduleUpdateOnFiber(currentRendierngFiber);
    };
    return [hook.memorizedState, fn];
}