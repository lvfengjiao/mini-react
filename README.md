**why virtum dom**
 avoid frequent update, and interact with dom, 
 DOM操作很慢， 轻微的操作可以引起重新排版，非常耗费性能， 相对于dom而言， js处理起来更快， 而且更简单。 通过diff算法对比新旧vdom的差异， 可以批量的， 最小化的执行dom的操作。

**where virtual dom**
levearge babel-loader to transfrom jsx to React.createElement
---

## lib -> create DomNode from VirtualDom and dom.appendChild()
1. vnode->node (isStr, isFunctionComponent, isClassComponent)
2. reconcileChilren
3. updateNode (not children, k!=children )

## lib-fiber -> create FiberNode from VirtualDom and schedulecreate, commitNode
1. render (jsx, container) --> create root fiber and currentFiber
2. performUnitWork -> return fiber;
3. updateHostComponent -- > createCUrrentnode and reconcileChildren to createFiber list
4. commitNode --> appendChildren

## lib-fiber-hook -> enrich fiber node for Fiber hook  and schedule create
1. 